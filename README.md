# Docker Basic Commands

## Commands

### Run Docker

`docker container run --publish 8080:80 --detach nginx:latest`

`--detach` - container works in the background

`--name` - our container name

`--publish 8080:80` - container port forwarding from `80` to `8080`

`:latest` - last version of container

### Checking working containers

`docker container ls`

### Stoping working container

`docker container stop id/name`

### Checking all containers

`docker container ls -a`

### Removing container

`docker container rm id/name`

### Checking container logs

`docker container logs name/id`

### Elasticsearch

`docker container run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -d docker.elastic.co/elasticsearch/elasticsearch:6.5.4`

### Rabbitmq

`docker container run -d --name rabbit-in-the-hole -p 8080:15672 rabbitmq:3-management`

### reviewing processes

`docker container top` 

### download full configuration

`docker container inspect`

### livestreaming performace

`docker container stats

### Console in docker

`docker container run -it`

`i` interractive mode

`t` terminal simulation

### add operation

`exec`

### creating a network

`docker network create --driver=bridge skynet`

### add container to network

`docker network connect NETWORK CONTAINER`

### disconnect docker and network

`docker network disconnect NETWORK CONTAINER`

### delete network

`docker network rm skynet`

### MySQL

`docker container run --name wordpressdb -e MYSQL_ROOT_PASSWORD=wordpress -e MYSQL_DATABASE=wordpress -d mysql:5.7`

### Wordpress

`docker container run -e WORDPRESS_DB_PASSWORD=wordpress -d --name wordpress --link wordpressdb:mysql -p 80:80 wordpress`

### pushing docker on repo

`docker image push`


### login to dockerhub

`docker login`

### pull docker from repo

`docker image pulld`
